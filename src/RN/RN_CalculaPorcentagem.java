/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RN;

import javax.swing.JOptionPane;

/**
 *
 * @author Lucas
 */
public class RN_CalculaPorcentagem {
    
    double soma;
    double perOp1, perOp2, perOp3;
    
    public void calcPercRepresentante(double[] totalVotos){
        soma = totalVotos[0]+totalVotos[1]+totalVotos[2];
        perOp1 = (totalVotos[0]/soma)*100;
        perOp2 = (totalVotos[1]/soma)*100;
        perOp3 = (totalVotos[2]/soma)*100;
        
        JOptionPane.showMessageDialog(null, "Resultado quantidade de votos:\nRepresentante 1: "+(int)perOp1+"%\nRepresentante 2: "+(int)perOp2+"%\nRepresentante 3: "+(int)perOp3+"%");
    }
    
    public void calcPercCoordenador(double[] totalVotos){
        soma = totalVotos[3]+totalVotos[4]+totalVotos[5];
        perOp1 = (totalVotos[3]/soma)*100;
        perOp2 = (totalVotos[4]/soma)*100;
        perOp3 = (totalVotos[5]/soma)*100;
        
        JOptionPane.showMessageDialog(null, "Resultado quantidade de votos:\nCoordenador 1: "+(int)perOp1+"%\nCoordenador 2: "+(int)perOp2+"%\nCoordenador 3: "+(int)perOp3+"%");
    }
    
    public void calcPercDepartamento(double[] totalVotos){
        soma = totalVotos[6]+totalVotos[7]+totalVotos[8];
        perOp1 = (totalVotos[6]/soma)*100;
        perOp2 = (totalVotos[7]/soma)*100;
        perOp3 = (totalVotos[8]/soma)*100;
        
        JOptionPane.showMessageDialog(null, "Resultado quantidade de votos:\nChefeDpto 1: "+(int)perOp1+"%\nChefeDpto 2: "+(int)perOp2+"%\nChefeDpto 3: "+(int)perOp3+"%");
    }
    
    public void calcPercReitor(double[] totalVotos){
        soma = totalVotos[9]+totalVotos[10]+totalVotos[11];
        perOp1 = (totalVotos[9]/soma)*100;
        perOp2 = (totalVotos[10]/soma)*100;
        perOp3 = (totalVotos[11]/soma)*100;
        
        JOptionPane.showMessageDialog(null, "Resultado quantidade de votos:\nReitor 1: "+(int)perOp1+"%\nReitor 2: "+(int)perOp2+"%\nReitor 3: "+(int)perOp3+"%");
    }
    
    public void calcPercViceReitor(double[] totalVotos){
        soma = totalVotos[12]+totalVotos[13]+totalVotos[14];
        perOp1 = (totalVotos[12]/soma)*100;
        perOp2 = (totalVotos[13]/soma)*100;
        perOp3 = (totalVotos[14]/soma)*100;
        
        JOptionPane.showMessageDialog(null, "Resultado quantidade de votos:\nVice Reitor 1: "+(int)perOp1+"%\nVice Reitor 2: "+(int)perOp2+"%\nVice Reitor 3: "+(int)perOp3+"%");
    }
    
}
